# FeedBotX - An RSS Feed Bot for XMPP

The bot can listen on a JID and allows users to subscribe to a predefined feed
via chat commands. The bot has a self-explaining help for users.

## Configure

Edit `~/.config/kaidan/feedbotx.conf`:

```toml
[xmpp]
jid=hello@domain.tld
password=<plaintext password>

[content]
feed=https://link-to-your-feed.xml
# feed update interval in seconds
poll-interval=30
```

Start the bot and write a message to it and it will do the rest!

## Build

```bash
git clone https://invent.kde.org/lnj/feedbotx
cd feedbotx
mkdir build
cd build
cmake .. -GNinja
ninja
```

### Dependencies
 * `Qt` (>= 5.11)
 * `QXmpp` (>= 1.0)

Only needed for building:
 * `cmake`
 * `ninja` or `make`
 * a C++ compiler for building

## Systemd unit (example)

```
[Unit]
Description=XMPP RSS Feed Bot

[Service]
Type=simple
ExecStart=/usr/local/bin/feedbotx
User=feedbot

[Install]
WantedBy=multi-user.target
```
