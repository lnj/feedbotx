// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "feedpoller.h"

#include <chrono>

#include <QDomDocument>
#include <QFutureInterface>
#include <QNetworkReply>
#include <QSettings>

using namespace std::chrono_literals;

FeedPoller::FeedPoller(Database *database, QNetworkAccessManager *netManager, QObject *parent)
    : QObject(parent),
      m_netManager(netManager),
      m_database(database)
{
    QSettings settings;

    m_feedUrl = settings.value("content/feed").toString();

    m_lastItem = m_database->lastItem(1);

    m_feedTimer.setInterval(settings.value("content/poll-interval", 30).toInt() * 1000);
#if QT_VERSION < QT_VERSION_CHECK(5, 12, 0)
    connect(&m_feedTimer, &QTimer::timeout, this, [this]() {
        poll(false);
    });
#else
    m_feedTimer.callOnTimeout(this, [this]() {
        poll(false);
    });
#endif
    m_feedTimer.start();
}

auto FeedPoller::requestFeed() -> QFuture<FeedResult>
{
    return get<FeedResult>(QNetworkRequest(m_feedUrl), [](QNetworkReply *reply) -> FeedResult {
        const auto data = reply->readAll();
        if (auto feed = RssFeed::fromXml(data)) {
            return *feed;
        }
        return std::pair(QNetworkReply::ContentNotFoundError, QStringLiteral("Server returned invalid RSS feed."));
    });
}

void FeedPoller::poll(bool initial)
{
    handleFuture<FeedResult>(requestFeed(), [this, initial](FeedResult result) {
        if (auto error = std::get_if<Error>(&result)) {
            qDebug() << "[FeedPoller] Error fetching feed:" << error->first << error->second;
        } else {
            const auto feed = std::get<RssFeed>(result);
            const auto processedItems = processFeedItems(feed.items());
            if (!initial) {
                emit newItemsReceived(std::move(processedItems));
            }
        }
    });
}

QVector<MessageItem> FeedPoller::processFeedItems(QVector<RssItem> items)
{
    QVector<MessageItem> newItems;
    QVector<QString> newItemIds;
    const auto currentItemIds = m_database->itemIds(1);

    for (const auto &item : std::as_const(items)) {
        if (currentItemIds.contains(item.guid())) {
            continue;
        }

        qDebug() << " *" << item.title();
        newItemIds << item.guid();

        MessageItem message;

        QDomDocument doc;
        auto description = item.description().replace("<br/>", "\n");

        const auto content =
                QStringLiteral("<content>") +
                description +
                QStringLiteral("</content>");

        if (doc.setContent(content)) {
            message.body = doc.documentElement().text();
        } else {
            message.body = description;
        }
        message.id = item.guid();

        newItems << std::move(message);
    }

    if (!newItems.isEmpty()) {
        qDebug() << "New items:" << newItems.size();
        // TODO: reverse items: oldest item is sent first
        m_lastItem = newItems.front();
        m_database->setLastItem(1, *m_lastItem);
        m_database->addItemIds(1, newItemIds);
    }
    return newItems;
}

std::optional<MessageItem> FeedPoller::lastItem() const
{
    return m_lastItem;
}
