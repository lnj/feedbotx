// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <QCommandLineParser>
#include <variant>

class CommandLineParser : public QCommandLineParser
{
public:
    struct Ok {};
    struct Error {};
    struct Login { QString jid; };

    using Result = std::variant<Ok, Error, Login>;

    CommandLineParser();

    Result process(const QCoreApplication &app);

private:
    const QCommandLineOption m_helpOption;
    const QCommandLineOption m_versionOption;
    const QCommandLineOption m_loginOption;
};

