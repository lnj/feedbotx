// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "database.h"

#include <QDebug>
#include <QDir>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QStandardPaths>

QSqlField createSqlField(const QString &key, const QVariant &val)
{
    QSqlField field(key, val.type());
    field.setValue(val);
    return field;
}

void prepareQuery(QSqlQuery &query, const QString &sql)
{
    if (!query.prepare(sql)) {
        qDebug() << "Failed to prepare query:" << sql;
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query)
{
    if (!query.exec()) {
        qDebug() << "Failed to execute query:" << query.executedQuery();
        qFatal("QSqlError: %s", qPrintable(query.lastError().text()));
    }
}

void execQuery(QSqlQuery &query, const QString &sql)
{
    prepareQuery(query, sql);
    execQuery(query);
}

void execQuery(QSqlQuery &query,
               const QString &sql,
               const QVector<QVariant> &bindValues)
{
    prepareQuery(query, sql);

    for (const auto &val : bindValues) {
        query.addBindValue(val);
    }

    execQuery(query);
}

void execQuery(QSqlQuery &query,
                      const QString &sql,
                      const QMap<QString, QVariant> &bindValues)
{
    prepareQuery(query, sql);

    const QStringList bindKeys = bindValues.keys();
    for (const auto &key : bindKeys)
        query.bindValue(key, bindValues.value(key));

    execQuery(query);
}

Database::Database()
{
    if (m_database) {
        m_database->close();
    }
}

void Database::openDatabase()
{
    Q_ASSERT(!m_database.has_value());

    m_database = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"), QStringLiteral("main"));
    if (!m_database->isValid()) {
        qFatal("Cannot add database: %s", qPrintable(m_database->lastError().text()));
    }

    const auto writeDir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!writeDir.mkpath(QLatin1String("."))) {
        qFatal("Failed to create writable directory at %s", qPrintable(writeDir.absolutePath()));
    }

    // Ensure that we have a writable location on all devices.
    const auto fileName = writeDir.absoluteFilePath(QStringLiteral("data.sqlite"));
    // open() will create the SQLite database if it doesn't exist.
    m_database->setDatabaseName(fileName);
    if (!m_database->open()) {
        qFatal("Cannot open database: %s", qPrintable(m_database->lastError().text()));
    }

    auto query = createQuery();
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS feeds ("
        "id INTEGER NOT NULL, "
        "url TEXT NOT NULL, "
        "PRIMARY KEY(id))");
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS feed_item_ids ("
        "feedId INTEGER NOT NULL, "
        "itemId TEXT NOT NULL, "
        "PRIMARY KEY(feedId, itemId))");
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS last_items ("
        "feedId INTEGER NOT NULL UNIQUE, "
        "body TEXT, "
        "itemId TEXT)");
    execQuery(
        query,
        "CREATE TABLE IF NOT EXISTS subscriptions ("
        "feedId INTEGER NOT NULL, "
        "userJid TEXT NOT NULL, "
        "FOREIGN KEY(feedId) REFERENCES feeds(id))"
    );
}

bool Database::isSubscribed(const QString &jid)
{
    auto query = createQuery();
    execQuery(
        query,
        "SELECT 1 FROM subscriptions WHERE userJid = :1",
        { jid }
    );

    return query.next();
}

void Database::addSubscription(const QString &jid)
{
    auto query = createQuery();
    execQuery(
        query,
        "INSERT INTO subscriptions VALUES (1, :1)",
        { jid }
    );
}

void Database::removeSubscription(const QString &jid)
{
    auto query = createQuery();
    execQuery(
        query,
        "DELETE FROM subscriptions WHERE userJid = :1",
        { jid }
                );
}

QVector<QString> Database::subscriptions(uint feedId)
{
    auto query = createQuery();
    execQuery(
        query,
        "SELECT userJid FROM subscriptions WHERE feedId = :1",
        { feedId }
    );

    QVector<QString> userJids;
    const auto rec = query.record();
    const auto indexUserJid = rec.indexOf("userJid");

    while (query.next()) {
        userJids.append(query.value(indexUserJid).toString());
    }
    return userJids;
}

std::optional<MessageItem> Database::lastItem(uint feedId)
{
    auto query = createQuery();
    execQuery(
        query,
        "SELECT * FROM last_items WHERE feedId = :1",
        { feedId }
    );

    if (query.next()) {
        const auto rec = query.record();
        return MessageItem {
            query.value(rec.indexOf("itemId")).toString(),
            query.value(rec.indexOf("body")).toString(),
        };
    }
    return std::nullopt;
}

void Database::removeLastItem(uint feedId)
{
    auto query = createQuery();
    execQuery(query, "DELTE FROM last_items WHERE feedId = :1", { feedId });
}

void Database::setLastItem(uint feedId, const MessageItem &item)
{
    auto query = createQuery();
    execQuery(
        query,
        "INSERT OR REPLACE INTO last_items VALUES (:1, :2, :3)",
        { feedId, item.body, item.id }
    );
}

void Database::addItemIds(uint feedId, const QVector<QString> &itemIds)
{
    m_database->transaction();
    auto query = createQuery();
    prepareQuery(
        query,
        "INSERT OR IGNORE INTO feed_item_ids VALUES (:1, :2)"
    );
    for (const auto &id : itemIds) {
        query.addBindValue(feedId);
        query.addBindValue(id);
        execQuery(query);
    }
    m_database->commit();
}

auto Database::itemIds(uint feedId) -> QVector<QString>
{
    auto query = createQuery();
    execQuery(
        query,
        "SELECT * FROM feed_item_ids WHERE feedId = :1",
        { feedId }
    );

    QVector<QString> itemIds;
    const auto rec = query.record();
    const auto indexItemId = rec.indexOf("itemId");
    while (query.next()) {
        itemIds.append(query.value(indexItemId).toString());
    }
    return itemIds;
}

QSqlQuery Database::createQuery()
{
    return std::move(QSqlQuery(*m_database));
}
