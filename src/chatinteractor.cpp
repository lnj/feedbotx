// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "chatinteractor.h"

#include <QXmppMessage.h>
#include <QXmppUtils.h>

#include "feedpoller.h"

ChatInteractor::ChatInteractor(XmppClient *client, FeedPoller *feedPoller, Database *database, QObject *parent)
    : QObject(parent),
      m_client(client),
      m_feedPoller(feedPoller),
      m_database(database)
{
    connect(client, &QXmppClient::messageReceived, this, &ChatInteractor::onMessageReceived);
}

void ChatInteractor::onMessageReceived(const QXmppMessage &message)
{
    const auto body = message.body();
    if (body.isEmpty()) {
        return;
    }
    const auto bareJid = QXmppUtils::jidToBareJid(message.from());

    auto send = [&](const QString &message, const QString &id = {}) {
        m_client->send(bareJid, message, id);
    };
    auto sendLast = [&](const MessageItem &item) {
        send(QStringLiteral("This is the latest post:"));
        send(item.body, item.id);
    };

    if (body == u"/subscribe" || body == u"/sub") {
        if (m_database->isSubscribed(bareJid)) {
            send(QStringLiteral("You're already subscribed! :)"));
        } else {
            m_database->addSubscription(bareJid);
            send(QStringLiteral("I subscribed you to the feed. Here you go!"));

            if (const auto lastItem = m_feedPoller->lastItem()) {
                sendLast(*lastItem);
            }
        }
    } else if (body == u"/unsubscribe" || body == u"/unsub") {
        if (m_database->isSubscribed(bareJid)) {
            m_database->removeSubscription(bareJid);
            send(QStringLiteral("I unsubscribed you from the feed."));
        } else {
            send(QStringLiteral("You're not subscribed to the feed!"));
        }
    } else if (body == u"/last") {
        if (const auto lastItem = m_feedPoller->lastItem()) {
            sendLast(*lastItem);
        } else {
            send(QStringLiteral("Sorry, I couldn't get the latest post. :("));
        }
    } else {
        send(QStringLiteral(R"(Sorry, no idea what that means o.O. I understand:
* `/sub` or `/subscribe` to subscribe to the feed
* `/unsub` or `/unsubscribe` to unsubscribe from the feed
* `/last` to get the latest post)"));
    }
}
