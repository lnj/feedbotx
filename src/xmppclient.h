// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <QXmppClient.h>

#include "database.h"

class XmppClient : public QXmppClient
{
    Q_OBJECT

public:
    explicit XmppClient(QObject *parent = nullptr);
    ~XmppClient();

    void login();
    void initialLogin(const QString &jid);

    void send(const QString &jid, const QString &body, const QString &id = {});

private:
    Q_SLOT void onConnectionStateChanged(QXmppClient::State state);

    void readSettings();
    void saveSettings();

    QString m_jid;
    QString m_password;
    QXmppLogger *m_logger;
};
