// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "commandlineparser.h"
#include <QCoreApplication>

CommandLineParser::CommandLineParser()
    : m_helpOption(addHelpOption()),
      m_versionOption(addVersionOption()),
      m_loginOption(QStringLiteral("login"),
                    QStringLiteral("Asks you for a password and saves the credentials."),
                    QStringLiteral("jid"))
{
    addOption(m_loginOption);
}

auto CommandLineParser::process(const QCoreApplication &app) -> Result
{
    QCommandLineParser::process(app);

    if (isSet(m_helpOption)) {
        showHelp();
    } else if (isSet(m_versionOption)) {
        showVersion();
    } else if (isSet(m_loginOption)) {
        if (const auto jid = value(m_loginOption); !jid.isEmpty()) {
            return Login { jid };
        }
        return Error();
    }
    return Ok();
}
