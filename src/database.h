// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <optional>

#include <QDateTime>
#include <QVector>
#include <QSqlDatabase>

struct MessageItem
{
    QString id;
    QString body;
};

class Database
{
public:
    Database();

    void openDatabase();

    bool isSubscribed(const QString &jid);
    void addSubscription(const QString &jid);
    void removeSubscription(const QString &jid);
    auto subscriptions(uint feedId = 1) -> QVector<QString>;

    auto lastItem(uint feedId = 1) -> std::optional<MessageItem>;
    void removeLastItem(uint feedId = 1);
    void setLastItem(uint feedId, const MessageItem &item);

    void addItemIds(uint feedId, const QVector<QString> &itemIds);
    auto itemIds(uint feedId) -> QVector<QString>;

private:
    QSqlQuery createQuery();

    std::optional<QSqlDatabase> m_database;

    QVector<QString> m_subscriptions;
};
