// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "rssfeed.h"

#include <QDomDocument>

std::optional<RssItem> RssItem::fromXml(const QDomElement &itemElement)
{
    auto value = [&](const QString &child) {
        return itemElement.firstChildElement(child).text();
    };

    RssItem item;
    item.m_title = value("title");
    item.m_description = value("description");
    item.m_link = value("link");
    item.m_guid = value("guid");
    item.m_pubDateString = value("pubDate");
    return item;
}

QString RssItem::title() const
{
    return m_title;
}

void RssItem::setTitle(const QString &title)
{
    m_title = title;
}

QString RssItem::description() const
{
    return m_description;
}

void RssItem::setDescription(const QString &description)
{
    m_description = description;
}

QString RssItem::link() const
{
    return m_link;
}

void RssItem::setLink(const QString &link)
{
    m_link = link;
}

QString RssItem::guid() const
{
    return m_guid;
}

void RssItem::setGuid(const QString &guid)
{
    m_guid = guid;
}

QString RssItem::pubDateString() const
{
    return m_pubDateString;
}

void RssItem::setPubDateString(const QString &pubDateString)
{
    m_pubDateString = pubDateString;
}

std::optional<RssFeed> RssFeed::fromXml(const QByteArray &rssBytes)
{
    QDomDocument doc;
    if (!doc.setContent(rssBytes)) {
        return std::nullopt;
    }

    return fromXml(doc.documentElement());
}

std::optional<RssFeed> RssFeed::fromXml(const QDomElement &rssElement)
{
    auto channel = rssElement.firstChildElement("channel");
    if (channel.isNull()) {
        return std::nullopt;
    }

    RssFeed feed;
    feed.m_title = channel.firstChildElement("title").text();
    feed.m_description = channel.firstChildElement("description").text();
    feed.m_link = channel.firstChildElement("link").text();
    feed.m_language = channel.firstChildElement("language").text();

    for (auto itemElement = channel.firstChildElement("item");
         !itemElement.isNull();
         itemElement = itemElement.nextSiblingElement("item")) {
        if (auto item = RssItem::fromXml(itemElement)) {
            feed.m_items << *item;
        }
    }

    return feed;
}

RssFeed::RssFeed()
{

}

QString RssFeed::title() const
{
    return m_title;
}

void RssFeed::setTitle(const QString &title)
{
    m_title = title;
}

QString RssFeed::description() const
{
    return m_description;
}

void RssFeed::setDescription(const QString &description)
{
    m_description = description;
}

QString RssFeed::link() const
{
    return m_link;
}

void RssFeed::setLink(const QString &link)
{
    m_link = link;
}

QString RssFeed::language() const
{
    return m_language;
}

void RssFeed::setLanguage(const QString &language)
{
    m_language = language;
}

QVector<RssItem> RssFeed::items() const
{
    return m_items;
}

void RssFeed::setItems(const QVector<RssItem> &items)
{
    m_items = items;
}
