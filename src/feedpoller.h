// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <functional>
#include <memory>
#include <variant>

#include <QObject>
#include <QTimer>
#include <QFuture>
#include <QFutureWatcher>
#include <QNetworkReply>

#include "rssfeed.h"
#include "database.h"

class Database;
class QNetworkRequest;
class QNetworkAccessManager;

class FeedPoller : public QObject
{
    Q_OBJECT

public:
    using Error = std::pair<QNetworkReply::NetworkError, QString>;
    using FeedResult = std::variant<RssFeed, Error>;

    explicit FeedPoller(Database *database, QNetworkAccessManager *netManager, QObject *parent = nullptr);

    QFuture<FeedResult> requestFeed();

    void poll(bool initial = false);
    Q_SIGNAL void newItemsReceived(QVector<MessageItem> items);

    std::optional<MessageItem> lastItem() const;

private:
    QVector<MessageItem> processFeedItems(QVector<RssItem> items);

    template<typename T>
    QFuture<T> reportResults(QNetworkReply *reply, std::function<T (QNetworkReply *)> processs)
    {
        auto interface = std::make_shared<QFutureInterface<T>>(QFutureInterfaceBase::Started);

        connect(reply, &QNetworkReply::finished, this, [=] () {
            interface->reportResult(processs(reply));
            interface->reportFinished();
            reply->deleteLater();
        });
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
        connect(reply, &QNetworkReply::errorOccurred, this, [=](QNetworkReply::NetworkError error) {
#else
        connect(reply, QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error), this, [=](QNetworkReply::NetworkError error) {
#endif
            interface->reportResult(std::pair(error, reply->errorString()));
            interface->reportFinished();
            reply->deleteLater();
        });

        return interface->future();
    }

    template<typename T>
    QFuture<T> get(QNetworkRequest &&request, std::function<T (QNetworkReply *)> processs)
    {
        return reportResults<T>(m_netManager->get(request), std::move(processs));
    }

    template<typename T, typename Handler>
    void handleFuture(QFuture<T> &&future, Handler handler)
    {
        auto *watcher = new QFutureWatcher<T>();
        connect(watcher, &QFutureWatcherBase::finished, this, [watcher, handler] {
            auto result = watcher->result();
            handler(result);
            watcher->deleteLater();
        });
        watcher->setFuture(future);
    }

    QNetworkAccessManager *m_netManager;
    QTimer m_feedTimer;
    Database *m_database;
    QString m_feedUrl;
    std::optional<MessageItem> m_lastItem;
};
