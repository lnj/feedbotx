// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "feeddistributor.h"

#include <QCoreApplication>

#include <QXmppMessage.h>

#include "commandlineparser.h"

FeedDistributor::FeedDistributor(QObject *parent)
    : QObject(parent),
      m_client(new XmppClient(this))
{
    m_database.openDatabase();
    m_poller = new FeedPoller(&m_database, new QNetworkAccessManager(this), this);
    m_chatInteractor = new ChatInteractor(m_client, m_poller, &m_database, this);

    connect(m_poller, &FeedPoller::newItemsReceived, this, &FeedDistributor::onNewItemsReceived);

    // parse command line options
    CommandLineParser parser;
    auto result = parser.process(*QCoreApplication::instance());

    if (const auto login = std::get_if<CommandLineParser::Login>(&result)) {
        m_client->initialLogin(login->jid);
        QCoreApplication::exit();
    } else {
        m_client->login();
    }
}

void FeedDistributor::onNewItemsReceived(const QVector<MessageItem> &items)
{
    const auto subscribedJids = m_database.subscriptions(1);
    std::for_each(items.rbegin(), items.rend(), [this, &subscribedJids](const MessageItem &feedItem) {
        for (const auto &subscriberJid : subscribedJids) {
            m_client->send(subscriberJid, feedItem.body, feedItem.id);
        }
    });
}
