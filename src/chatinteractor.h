// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include "xmppclient.h"

class FeedPoller;

class ChatInteractor : public QObject
{
    Q_OBJECT

public:
    explicit ChatInteractor(XmppClient *client, FeedPoller *feedPoller, Database *database, QObject *parent = nullptr);

private:
    Q_SLOT void onMessageReceived(const QXmppMessage &message);

    XmppClient *m_client;
    FeedPoller *m_feedPoller;
    Database *m_database;
};
