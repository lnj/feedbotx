// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "xmppclient.h"

#include <iostream>

#include <QSettings>

#include <QXmppUtils.h>
#include <QXmppMessage.h>
#include <QXmppRosterManager.h>

XmppClient::XmppClient(QObject *parent)
    : QXmppClient(parent),
      m_logger(new QXmppLogger(this))
{
    setLogger(m_logger);
    connect(this, &QXmppClient::stateChanged, this, &XmppClient::onConnectionStateChanged);
}

XmppClient::~XmppClient()
{
}

void XmppClient::login()
{
    readSettings();

    if (m_jid.isEmpty() || !m_jid.contains(u'@') || m_jid.size() < 3 || m_password.isEmpty()) {
        qFatal("No valid JID or password configured! Call with --login to save credentials.");
    }

    qDebug() << "Connecting as" << m_jid;
    QXmppConfiguration config;
    config.setJid(m_jid);
    config.setPassword(m_password);
    config.setAutoAcceptSubscriptions(true);
    connectToServer(config);
}

void XmppClient::initialLogin(const QString &jid)
{
    std::cout << "Logging in as " << jid.toStdString() << ".\n";
    std::string password;
    while (password.empty()) {
        std::cout << "Password: ";
        std::cin >> password;
    }

    m_jid = jid;
    m_password = QString::fromStdString(password);
    saveSettings();
}

void XmppClient::send(const QString &jid, const QString &body, const QString &id)
{
    QXmppMessage message;
    message.setId(id.isEmpty() ? QXmppUtils::generateStanzaHash() : id);
    message.setTo(jid);
    message.setBody(body);
    sendPacket(message);
}

void XmppClient::onConnectionStateChanged(QXmppClient::State state)
{
    switch (state) {
    case QXmppClient::ConnectingState:
        qDebug() << "[XMPP] Connecting...";
        break;
    case QXmppClient::ConnectedState:
        qDebug() << "[XMPP] Connected";
        break;
    case QXmppClient::DisconnectedState:
        qDebug() << "[XMPP] Disconnected";
        break;
    }
}

void XmppClient::readSettings()
{
    QSettings settings;
    m_jid = settings.value(QStringLiteral("xmpp/jid")).toString();
    m_password = settings.value(QStringLiteral("xmpp/password")).toString();
}

void XmppClient::saveSettings()
{
    QSettings settings;
    settings.setValue(QStringLiteral("xmpp/jid"), m_jid);
    settings.setValue(QStringLiteral("xmpp/password"), m_password);
}
