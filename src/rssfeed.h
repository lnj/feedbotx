// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <optional>

#include <QString>
#include <QVector>

class QDomElement;

class RssItem
{
public:
    static std::optional<RssItem> fromXml(const QDomElement &itemElement);

    QString title() const;
    void setTitle(const QString &title);

    QString description() const;
    void setDescription(const QString &description);

    QString link() const;
    void setLink(const QString &link);

    QString guid() const;
    void setGuid(const QString &guid);

    QString pubDateString() const;
    void setPubDateString(const QString &pubDateString);

private:
    QString m_title;
    QString m_description;
    QString m_link;
    QString m_guid;
    QString m_pubDateString;
};

class RssFeed
{
public:
    static std::optional<RssFeed> fromXml(const QByteArray &rssBytes);
    static std::optional<RssFeed> fromXml(const QDomElement &rssElement);

    RssFeed();

    QString title() const;
    void setTitle(const QString &title);

    QString description() const;
    void setDescription(const QString &description);

    QString link() const;
    void setLink(const QString &link);

    QString language() const;
    void setLanguage(const QString &language);

    QVector<RssItem> items() const;
    void setItems(const QVector<RssItem> &items);

private:
    QString m_title;
    QString m_description;
    QString m_link;
    QString m_language;
    QVector<RssItem> m_items;
};
