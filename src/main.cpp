// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <QCoreApplication>

#include "feeddistributor.h"

constexpr QStringView APPLICATION_NAME = u"feedbotx";
constexpr QStringView APPLICATION_VERSION = u"" PROJECT_VERSION;
constexpr QStringView ORGANISATION_NAME = u"kaidan";
constexpr QStringView ORGANISATION_DOMAIN = u"kaidan.im";

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName(APPLICATION_NAME.toString());
    app.setApplicationVersion(APPLICATION_VERSION.toString());
    app.setOrganizationName(ORGANISATION_NAME.toString());
    app.setOrganizationDomain(ORGANISATION_DOMAIN.toString());

    FeedDistributor distributor;

    return app.exec();
}
