// SPDX-FileCopyrightText: 2021 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <QObject>

#include "feedpoller.h"
#include "xmppclient.h"
#include "chatinteractor.h"

class FeedDistributor : public QObject
{
    Q_OBJECT

public:
    explicit FeedDistributor(QObject *parent = nullptr);

    Q_SLOT void onNewItemsReceived(const QVector<MessageItem> &items);

private:
    Database m_database;
    XmppClient *m_client;
    FeedPoller *m_poller;
    ChatInteractor *m_chatInteractor;
};
